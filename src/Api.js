const { SearchRequest, SearchResult, FoodItem } = require("./models");

const axios_default = require("axios").default;

class FdcApi {
  /**
   *
   * @param {string} key - API key
   * @param {({pretty: boolean}|boolean)} debug - `true` or {pretty: <boolean>} for pretty print
   */
  constructor(key, debug) {
    if (!key) {
      throw Error("No key passed to FDC API Constructor");
    }
    this.key = key;
    this.debug = debug;
    this.axios = axios_default.create({
      baseURL: "https://api.nal.usda.gov/fdc/v1",
      headers: { "Content-Type": "application/json" }
    });
  }

  /**
   * Search the FDC API for foods.
   *
   * @param {SearchRequest} searchRequest
   * @returns {Promise<SearchResult>}
   */
  async search({
    generalSearchInput,
    includeDataTypeList,
    ingredients,
    brandOwner,
    requireAllWords,
    pageNumber,
    sortField,
    sortDirection
  }) {
    const searchReq = new SearchRequest({
      generalSearchInput,
      includeDataTypeList,
      ingredients,
      brandOwner,
      requireAllWords,
      pageNumber,
      sortField,
      sortDirection
    });

    if (this.debug) {
      console.info(
        `[FDC API] POST /search -body ${JSON.stringify(
          searchReq,
          null,
          typeof this.debug === "object" && this.debug.pretty ? 2 : undefined
        )}\n------`
      );
    }

    const result = await this.axios
      .post(`/search?api_key=${this.key}`, searchReq)
      .then(r => r.data);

    return result ? new SearchResult(result, result) : null;
  }

  /**
   * Get a FoodItem by ID.
   *
   * @param {string} id - FDC ID
   * @returns {Promise<FoodItem>}
   */
  async getItem(id) {
    if (this.debug) {
      console.info(`[FDC API] GET /${id}`);
    }

    const result = await this.axios
      .get(`/${id}?api_key=${this.key}`)
      .then(r => r.data);

    return result ? new FoodItem(result, result) : null;
  }
}

module.exports = FdcApi;
