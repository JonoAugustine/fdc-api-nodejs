class SearchRequest {
  constructor({
    generalSearchInput,
    includeDataTypeList,
    ingredients,
    brandOwner,
    requireAllWords,
    pageNumber,
    sortField,
    sortDirection
  }) {
    this.generalSearchInput = generalSearchInput;
    this.includeDataTypeList = includeDataTypeList;
    if (ingredients) {
      ingredients.reduce((acc, cur) => `${acc} ${cur}`);
    }
    this.sortField = sortField;
    this.brandOwner = brandOwner;
    this.requireAllWords = requireAllWords;
    this.pageNumber = pageNumber;
    this.sortDirection = sortDirection;
  }
}

module.exports = SearchRequest;
