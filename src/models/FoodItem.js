/**
 *
 * @param {string} html
 */
const transformHighlightField = html => {
  // Pull out the text in <em> tags and return an array of strings
  return html.match(/(?<=<em>).+(?=<\/em>)/i);
};

/**
 * An FDC Food item.
 *
 * @since 1.0.0
 * @author Jonathan Augustine
 */
class FoodItem {
  /**
   *
   * @param {object} Raw
   * @param {object} raw - The raw response from the FDC API
   */
  constructor(
    {
      fdcId,
      description,
      scientificName,
      commonNames,
      additionalDescriptions,
      dataType,
      foodCode,
      gtinUpc,
      ndbNumber,
      publishedDate,
      brandOwner,
      ingredients,
      allHighlightFields,
      score
    },
    raw
  ) {
    /** The unique ID of this food */
    this.id = fdcId;
    /** The description of this food */
    this.description = description;
    /** The scientific name of the food */
    this.scientificName = scientificName;
    /** A list of common names for this food */
    this.commonNames = commonNames ? commonNames.split(/,/) : [];
    this.additionalDescriptions = additionalDescriptions;
    this.dataType = dataType;
    this.foodCode = foodCode;
    this.gtinUpc = gtinUpc;
    this.ndbNumber = ndbNumber;
    this.publishedDate = publishedDate ? new Date(publishedDate) : null;
    this.brandOwner = brandOwner;
    /** TODO Split this better */
    this.ingredients = ingredients ? ingredients.split(/,/) : [];
    /**	Fields that were found matching the criteria. */
    this.matches = allHighlightFields
      ? transformHighlightField(allHighlightFields)
      : [];
    /** Relative score indicating how well the food matches the search criteria. */
    this.score = score;
    /** The raw response from the FDC API */
    this.raw = raw;
  }
}

module.exports = FoodItem;
