const FoodItem = require("./FoodItem");

/**
 * FDC Search Result
 */
class SearchResult {
  /**
   *
   * @param {object} Raw
   * @param {Array<object>} Raw.foods
   * @param {object} Raw.foodSearchCriteria
   * @param {number} Raw.totalHits
   * @param {number} Raw.currentPage
   * @param {number} Raw.totalPages
   * @param {object} raw
   */
  constructor(
    { foodSearchCriteria, totalHits, currentPage, totalPages, foods },
    raw
  ) {
    /** A copy of the criteria that were used in the search */
    this.query = foodSearchCriteria;
    /** The total number of foods found matching the search criteria. */
    this.count = totalHits;
    /** The total number of pages found matching the search criteria. */
    this.pageCount = totalPages;
    /** The current page of results */
    this.page = currentPage;
    /**
     * The list of foods found matching the search criteria.
     * @type {Array<FoodItem>}
     */
    this.foods = foods.map(f => new FoodItem(f, f));
    this.raw = raw;
  }
}

module.exports = SearchResult;
