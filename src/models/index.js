module.exports = {
  FoodItem: require("./FoodItem"),
  SearchResult: require("./SearchResult"),
  SearchRequest: require("./SearchRequest")
};
