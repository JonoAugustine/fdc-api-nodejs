# FDC API NodeJS Example

1. Install dependencies (nodemon, fans, dotenv)

```
$ npm install
```

2. Get your key from [the FDC site](https://fdc.nal.usda.gov/api-key-signup.html)

3. Add your key to a .env

```
$ echo KEY=<your_key> > .env
```

4. Run a live-refreshing example

```
$ npm start
```