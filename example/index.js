require("dotenv").config();
const FdcApi = require("fans");

const api = new FdcApi(process.env.KEY, { pretty: true });

api
  .search({
    generalSearchInput: "chicken"
  })
  .then(sr => {
    console.log(JSON.stringify(sr.query));
    return sr.foods[0].id;
  })
  .then(fid => api.getItem(fid))
  .then(food => console.log(JSON.stringify(food, null, 2)))
  .catch(e => console.error(e));
