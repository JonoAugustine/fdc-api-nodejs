# FANS

## Fdc Api NodejS

A NodeJS wrapper around the FoodData Central API.

## Installation

```
$ npm i -S fans
```

https://www.npmjs.com/package/fans

## Dependencies

- Axios
